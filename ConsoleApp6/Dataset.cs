﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleApp6
{
    class Dataset
    {
        private List<List<string>> data; //list of lists of strings
       
        
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
       
        
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        
        
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }

        public Dataset(Dataset dataset)
        {
            this.data = new List<List<string>>(dataset.GetData().Count);
            foreach (List<string> subDataset in dataset.GetData())
            {
                List<string> newSubDataset = new List<string>(subDataset.Count);

                foreach (string SubString in subDataset)
                {
                    string newSubString = SubString;
                    newSubDataset.Add(newSubString);
                }
                this.data.Add(newSubDataset);
            }
        }

        public IList<List<string>> GetData()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        
        
        public void ClearData()
        {
            this.data.Clear();
        }
      
        
        public Prototype Clone()
        {
            Dataset clone = new Dataset(this);
            return (Prototype)clone;
        }




        public override string ToString()
        {

            StringBuilder stringBuilder = new StringBuilder();
            foreach (var subDataset in (this.GetData()))
            {
                foreach (var data in subDataset)
                {
                    stringBuilder.Append(data).Append(" ");
                }

                stringBuilder.Append("\n");
            }
            return stringBuilder.ToString();

        }
    }
}
//potrebno je duboko kopirati jer su liste reference pa bi plitkim kopiranjem mijenjali sami objekt jer plitko kopiranje stvori referencu na objekt