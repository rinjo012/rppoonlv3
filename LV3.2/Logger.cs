﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3._2
{
    class Logger
    {
        static Logger instance;
       
        private String filePath;
        public String FilePath { 
            private
            get { return filePath; } 
            set { this.filePath = value; } 
        }

        private Logger()
        {
            filePath = "C:\\Users\\Korisnik\\source\\repos\\ConsoleApp6\\Logger.txt";
        }


        static public Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }

        public void Log(String message) 
        {
            using (StreamWriter streamWriter = new StreamWriter(this.FilePath, true))
            {
                streamWriter.WriteLine(message);
            }
        }
    }
}


//Imamo jednu instancu logger objekta pa ako promijenimo filepath za jedan logger promijenit cemo za sve sto znaci da ce i drugi logger pisati u izmjenjenu datoteku