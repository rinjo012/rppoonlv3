﻿using System;

namespace LV3._2
{
    class Program
    {
        static void Main(string[] args)
        {


            Logger firstLogger = Logger.GetInstance();
            firstLogger.Log("FirstLogger");

            Logger secondLogger = Logger.GetInstance();
            secondLogger.Log("SecondLogger");
            secondLogger.FilePath = "C:\\Users\\Korisnik\\source\\repos\\ConsoleApp6\\Logger2.txt";
            secondLogger.Log("SecondLogger Number 2");
            firstLogger.Log("FirstLogger Number 2");
        }
    }
}
